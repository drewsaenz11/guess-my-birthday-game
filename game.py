from random import randint

player_name = input("Hello! What is your name? ")
num_guesses = 5
guess_number = 1

for date_guess in range(num_guesses):
    guess_number += 1
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)    

    print("Guess " + str(date_guess + 1) + " : " + player_name + 
    " were you " + "born on " + str(month_number) + " / " + str(year_number) + " ?")
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    else:
        print("Drat! Lemme try again!")


print("I have other things to do. Good Bye!")